## Introducción a las trobairitz y juglaresas

Las trobairitz: las talentosas mujeres trovadoras silenciadas por la historia 

La figura de los trovadores  es suficientemente conocida,pero las troibairitz
no. Estas trovadoras escribieron poesías de alta calidad,en la que se pueden
diferenciar de los trovadores.

Era un oficio reservado solo para hombres y las mujeres que querían formar 
parte, tenían que hacerlo bajo el pseudónimo del apellido del marido. Apenas 
unos veinte han podido escapar del olvido de la historia.  El concepto 
«trobairitz» se utilizó por primera vez en siglo XIII, y su significado era 
«componer».

Hasta ese momento, las pocas mujeres compositoras solo escribían música sacra,
siendo las trobairitz las primeras compositoras de música secular occidental.
Su obra solo se representaba en el ámbito privado, y su temática rompía con la
tradición poética del trovador. En este caso, las mujeres reclaman al amado su
afecto. Algo muy atrevido y arriesgado en la época.  Estas nobles mujeres, a
través de sus versos, hablan de la felicidad, la desazón, el deseo, la ansiedad;
sentimientos provocados por un amor ferviente.  En estos versos las trovadoras
buscaban perfección y belleza, describiendo los ideales que esperaban encontrar
en un hombre para amarlo.  Este amor era dirigido a caballeros, trovadores y,
también, a otras mujeres. Nunca a sus esposos. La lengua utilizada en sus
poemas era lengua occitana, lengua romance hablada en Francia, Italia y España.
De entre todas ellas sobresale María Pérez de Balteira, musa e inspiración 
de muchos trovadores de la corte del rey.  Otros nombres destacados fueron:
Beatriz de Día, esposa de Guilhen de Petieu; Alamanda de Castelnau; Maria de
Ventadorn

