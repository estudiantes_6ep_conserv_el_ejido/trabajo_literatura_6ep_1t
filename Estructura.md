### Estructura y trasfondo  de la pieza *En greu esmay*

Como hemos dicho anteriormente, esta pieza es del género *cansó*, y por lo
tanto el tema tratado en la misma es el amor cortés.

Este tema es uno de los principales utilizados por los trovadores, que basaban
sus composiciones en las temáticas amor-naturaleza o lealtad-heroísmo.

Los dos primeros temas tienen su referencia en el amor cortés y se utilizaban
en las veladas cortesanas de salón, mientras que el segundo es descendiente
de los cantares de gesta de la alta edad media.

De la autora de la pieza, Clara d'Anduza, no se conservan grandes datos
biográficos y, de hecho, esta pieza es la única que se conserva completa de
ella. Se cree que toma el nombre de Anduza por ser originaria de dicha comuna
francesa o por ser familia de los señores del lugar.

La relación que tuvo con Uc de Sant-Circ (trovador, biógrafo y gramático)
asi como con otros trovadores es lo que nos ha permitido fechar su producción
poética.

Esta pieza se conserva en el cancionero C y cuenta el amor entre dos amantes a
los que los *lauzengieros* han separado.

Uc de Sant Circ explica algo del trasfondo de la misma a través de una 
*razó*: el amante de Clara se habría alejado de la misma por culpa de una
tercera persona pero, finalmente, se habrían reconciliado mediante la ayuda de
una amiga que se cree podría ser Azalais de Altier.

Por otro lado, a lo largo de la historia, varios historiadores han referenciado
a Clara d'Anduza en sus escritos, es el caso de Coll i Vehí que transcribe
cuatro versos de la pieza y considera a Clara «discreta» y «casada», además
de pensar que *En greu esmay* alude a una experiencia real que «debieron
alarmar al marido». Coll i Vehí fue un historiador que escribió esto en
1861.

Victor Balaguer también la menciona en su obra, entro otras como la mucho más
conocida Condesa de Día. Ya en el siglo XX, en 1910, Rilke la situa en un
mismo grupo que a la anterior: el de grandes enamoradas.

#### Estructura formal de la cansó

La *cansó* comparte estructura con el otro gran género trovadoresco, el
*sirventés*, ambos están compuestos por un número de versos entre 40 y 60,
divididos en coblas (estrofas) de entre 6 y 10 versos.

Dividida en tres partes, la *cansó* nunca supera las 6 coblas. En la primera
parte, el exordio, es donde se explica el proposito de la composición.

#### Estructura y caracterización de la pieza *En greu esmay*

La pieza esta construida en tres coblas (párrafos) y una tornada, con un total
de 28 versos, una duración algo más corta de lo normal en este género. En las
dos audiciones que vamos a escuchar se descubren diferencias de interpretación
aunque las dos giran en torno a esta estructura básica.

Escrita en idioma de Oc, Anduza se encuentra en la región de Languedoc, el
término *amics* (mencionado dos veces, una de ella como *bels amics*) es el que
hace referencia al amado, aunque según ciertas fuentes no siempre es así en 
todas las composiciones, ya que puede hacer referencia a una relación afectiva
pero sin intimidad. 

En *es greu esmay* encontramos un fuerte dolor de la autora provocado por el 
distanciamiento del amado, provocado como ya se ha mencionado por los 
*lauzengieros* y que podemos encontrar expresado en los versos:

> «qar vos q’eu am mais que res qu’el mon sia  
> an fait de me departir e lonhar,  
> si q’ieu no.us puesc vezer ni remirar,  
> don muer de dol, d’ira e de feunia.»  

dentro de la primera cobla, refiriendo ya el tema principal de la composición.

Hay que añadir que, al igual que sucede con los trovadores, rara vez las 
trobairitz aludian al amigo por su nombre, cosa que sucede en esta pieza, pero
si se hacen referencias a sus cualidades, cosa que no sucede en esta ocasión.

En otras fuentes se cita como amado al nombrado Uc de Sant Circ, que ha 
traicionado a Clara d'Anduza y, gracias a la citada Azalais de Altier, 
consiguen reconciliarse, aunque eso no se expresa en la pieza.

Otro rasgo llamativo de las trobairitz es la nula referencia, dentro del corpus
que se conserva, a rasgos físicos a excepción de una composición de Azalais
de Porcairages; las trobairitz siempre ponen el acento en lo anímico.

Es *en greu esmay* en general una *cansó* donde se expresa un lamento por el
infortunio de la separación del amado debido a las mencionadas intrigas,
aunque, en algún momento parece sugerir esperanza en la reconciliación. 
Este infortunio incluso llega a producir inquietud y rabia:

> «Amics, tan ai d'ir e de feunia  
> quar no vos vey [...]».

No se hace dudar, por tanto, que *en greu esmay* es una canción de desamor.

