## Recuperación de las obras de las trobairitz y juglaresas

Se conservan muy pocas fuentes sobre las trobairitz y gran parte de la
información ha sido obtenida de sus cancioneros “razos” o de sus 
biografías. Sin embargo, esta última fuente no es muy fiable, debido a que se
mezclan los hechos que les ocurrieron realmente con aquellos que solo tuvieron 
lugar en sus poemas.

En total poseemos alrededor de 20 nombres de los s. XII y XIII y aproximadamente
32 composiciones. Esta cifra es bastante baja si la comparamos con la de su
equivalentes masculinos, de los que poseemos alrededor de 460 nombres y 2600
composiciones.

Hablamos del número 32 como una aproximación debido a que en realidad la cifra
oscila entre 23 y 46 composiciones. El motivo principal es porque no se tiene
del todo claro que composiciones fueron realmente compuestas por mujeres y
cuales tiene como protagonista a una mujer y fueron escritas por hombres, sobre
todo en caso de composiciones anónimas.

Los temas se basaban en leyendas, relatos y mitos o experiencias basadas en su
entorno. La trova más antigua fue escrita en 1150 por Tibors de Sarenom y recibe
el nombre de “Bels dou amic”. Lamentablemente, casi todas han sufrido alguna
modificación en su dotación musical excepto “A Chantar” de la Condesa de Día o
Beatriz de Día que es la única que se conserva intacta.

En cuanto a las juglaresas tenemos muy poca información de ellas y de sus obras
hoy en día, provocando que únicamente se conserve algunas descripciones. Esto
probablemente se deba la asociación que existía entre ellas y el pecado que
representaban, la lujuria.

Una de las más famosas es la juglaresa María Pérez de Balteira. A diferencia
del resto de juglaresas pertenecía a una familia de nobles y fue conocida por
estar ligada a las figuras de Fernando III «El Santo» y su hijo Alfonso X «El
Sabio». Además fue protagonista de varias cántigas escritas por diversos
autores.

