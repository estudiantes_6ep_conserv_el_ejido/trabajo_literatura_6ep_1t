### Audición de la obra elegida

[Audición en Youtube](
https://www.youtube.com/watch?v=6UjOzk-bZMk&feature=youtu.be) a cargo de Flor
Enversa.
 
*   Indumentaria e instruments (que ropa llevaban, y qué tocaban las trobairitiz? El video nos muestra una actuación históricamente correcta?
 
*   Como se ha recuperado?

### 2ª audición 

[Audición en Youtube](
https://www.youtube.com/watch?v=kC5OLEq7s8A) a cargo de Motus Harmonicus.

*  Comparación

### Otras versiones

Existe otra versión más a cargo de Bela Domna, también disponible en el
servicio de [Youtube](
https://www.youtube.com/watch?v=221aeJJaMRc&feature=youtu.be).

