## Géneros tratados por las Trobairitz y juglaresas

Géneros Trovadorescos

1. Cansó: Todas las obras escritas en verso para ser cantadas que trataban 
    de una forma u otra sobre el amor.
2. Sirventes: En su origen era una «canción de servicio» para un señor 
    noble, normalmente cantando por un trovador criado o sirviente, pero luego
    pasa a ser una canción que trataba sobre cualquier tema no relacionado con
    el amor, aunque la mayoría trataban temas sobre la política. Dos 
    subgéneros de bastante importancia eran el enueg (enojo) y el planh 
    (lamento). 
3. Tensó (o disputa): Se trata de una forma en la que dos trobairitiz debaten 
   cotestandose la una a la otra alternando estrofas, muchas veces no muy 
   amigablemente sobre un tema en concreto.
4. Partimen o joch partit: Es un debate en la que la trobairitz propone 
   alternartivas y deja que el adversario escoja la que quiere defender.
5. Alba: Trata sobre el lamento de unos amados al tener que separarse al 
   amanecer.
6. Balada: Canción para ser bailada.
 
La audición que hemos elegido es una cansó; la letra traducida al 
castellano es la siguiente:


*En grave angustia y en grave tormento*

(I cobla)  
En grave angustia y en grave tormento  
han puesto mi corazón, y en gran error,  
los maledicientes y los falsos adivinos  
que envilecen alegría y juventud,  
ya que os han separado y alejado de mí  
a vos, a quien amo más que cosa alguna,  
así que no puedo veros y admiraros  
por lo que muero de dolor, de ira y de rencor.  

(II cobla)  
Quien me censura y me aleja de vuestro amor,  
en nada puede hacer mi corazón mejor  
ni el dulce deseo que tengo de vos mayor,  
ni la añoranza ni el querer ni la inclinación.  
Y mi peor enemigo, si le escucho decir bien  
de vuestro amor, se me convierte en querido;  
y si habla mal de vos, jamás podrá decir o hacer  
nada que sea grato para mí.  

(III cobla)  
Ahora, no tengáis ya, bello amigo, temor  
de que abrigue hacia vos sentimientos engañosos  
o de que cambie por causa de otro amor,  
aunque me lo suplicara un centenar;  
ya que el amor que por vos me tiene en su poder,  
quiere que mi corazón sea vuestro y os pertenezca,  
y así será: y si yo pudiera quitarme el corazón,  
aquel que lo tuviera nunca lo poseería.  

(Tornada)  
Amigo, tan airada y triste estoy, porque no os veo,  
que cuando creo cantar  
lloro y suspiro, por lo que no puedo decir,  
en mis estrofas, lo que el corazón querría.  

Y la letra original:

*En greu esmai et en greu pessamen*

(I)  
En greu esmay et en greu pessamen  
an mes mon cor et en granda error  
li lauzengier e.l fals devinador,  
abayssador de ioy e de ioven,  
qar vos q’eu am mais que res qu’el mon sia  
an fait de me departir e lonhar,  
si q’ieu no.us puesc vezer ni remirar,  
don muer de dol, d’ira e de feunia.  

(II)  
Selh que.m blasma vostr’amor ni.m defen  
no podon far en re mon cor mellor,  
ni.l dous dezir qu’ieu ai de vos maior  
ni l’enveya ni.l dezir ni.l talen;  
e non es hom –tan mos enemicx sia–  
si.l n’aug dir ben, que no.l tenha en car  
e, si’n ditz mal, mais no.m pot dir ni far  
neguna re que a plazer me sia.  

(III)  
Ia no.us donetz, bels amicx, espaven  
que ia ves vos aia cor trichador,  
ni qu’ie.us camge per nul autr’amador,  
si.m pregavon d’autras donas un cen;  
qu’amors que.m te per vos en sa bailia  
vol que mon cors vos estuy e vos gar,  
e farai o; e, s’ieu pogues emblar  
mon cors, tals l’a que iamais non l’auria.  

(Tornada)  
Amicx, tan ai d’ira e de feunia  
quar no vos vey, que quant yeu cug chantar  
planh e sospir, per qu’ieu no puesc so far  
a mas coblas que.l cors complir volria.  

