![Build Status](https://gitlab.com/pages/gitbook/badges/master/build.svg)

---

Trabajo de literatura: Las trobairitiz

---

## Estructura del trabajo


1. Introducción a las trobairitiz y juglaresas
    1. Las diferencias entre trobairitz y juglaresas
    2. No pondremos audición de juglaresas porque no hay ninguno 
       que se conserve hoy en día.
2. Cuales son los géneros que componían/cantaban y cual es la 
   que sale en la audición propuesta.
    1. Significado de la letra de la canción.
3. Recuperación de la obra de las trobairitiz y juglaresas.
4. Audición de la obra elegida
    1. Indumentaria e instruments (que ropa llevaban, y qué tocaban las 
       trobairitiz? El video nos muestra una actuación históricamente correcta?
    2. ¿Como se ha recuperado?
    3. 2ª audición
        - Comparación
5. Estructura de la pieza.