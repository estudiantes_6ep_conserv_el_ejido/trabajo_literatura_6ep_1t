## Bibliografía y referencias

- https://sombrasydelirios.com/2018/12/01/en-greu-esmai-et-en-greu-pessamen-clara-danduza/
- http://mlozar.blogspot.com/2012/07/trobairitz.html
- https://www.cinconoticias.com/trobairitz-mujeres-trovadoras-silenciadas-por-la-historia/
- http://laropamedieval.blogspot.com/2009/09/la-musica-en-la-edad-media-juglares.html?m=1
- https://occitanica.eu/files/original/72967fb6c192d2f4f9703eec55c35a94.pdf
- [Las trobairitz provenzales en el fin de siglo](
  https://ddd.uab.cat/pub/lectora/20139470n3/20139470n3p27.pdf)
- Sánchez Trigo, Elena. (1). [Caracterización del amigo en Las Trobairitz.](
  https://revistas.um.es/estudiosromanicos/article/view/80441/77681) Estudios 
  Románicos, 5, 1293-1306. Recuperado a partir de 
  https://revistas.um.es/estudiosromanicos/article/view/80441
- [La lírica trovadoresca](
  https://sites.google.com/site/losminutosqueprecedenalsueno/home/lirica-trovadoresca)
- [¿Qué cantaban los trovadores?, Musicaantigua.com](
  http://www.musicaantigua.com/que-cantaban-los-trovadores/)
- [Canción (trovador), Wikipedia](
  https://es.wikipedia.org/wiki/Canci%C3%B3n_(trovador))
- [Sirventés, Wikipedia](
  https://es.wikipedia.org/wiki/Sirvent%C3%A9s)
- [Les trobairitz](
  http://www.trob-eu.net/ca/les-trobairitz.html)
- Viñez Sánchez, Antonia. La voz disidente de las Trobairitz.

