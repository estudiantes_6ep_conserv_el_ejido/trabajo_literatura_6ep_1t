# Summary

* [Presentación](README.md)
* [Introducción a las trobairitz y juglaresas](
   Intro_trobairitz_juglaresas.md)
* [Géneros musicales tratados por trobairitz y juglaresas](Generos.md)
* [Recuperación de la obra de las trobairitz y juglaresas](Recuperación_obras.md)
* [Estructura y caracterización de la pieza *En greu esmai*](Estructura.md)
* [Audición de la pieza *En greu esmai* de Clara d'Anduza](Audiciones.md)
* [Bibliografía y referencias](Referencias.md)

